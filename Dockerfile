FROM golang:1.16 as builder
RUN mkdir /build
WORKDIR /build
ADD . .
RUN go build -o main


FROM ubuntu:18.04 AS bch-back
EXPOSE 8091
RUN mkdir /os
WORKDIR /os
COPY --from=builder /build/main .

#RUN apt update && apt install -y git ca-certificates && update-ca-certificates # for https
CMD [ "./main" ]

