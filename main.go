package main

import (
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strconv"
	"time"

	"github.com/0LuigiCode0/library/logger"
	"github.com/gorilla/mux"
)

// Block структура блока
type Block struct {
	Index     int64
	TimeStamp time.Time
	BlockData []string
	PrevHash  string
	Hash      string
}

var Blockchain []Block

type Server struct {
	Addr   string
	Pin    string
	router *mux.Router
	log    *logger.Logger
}

// RequestData данные блока в запросе
type RequestData struct {
	Data string `json:"data"`
}

func main() {
	log := logger.InitLogger("")
	InitGenesisBlock()
	log.Service("generate genesis block")
	server := InitServer(log)

	if err := server.Start(); err != nil {
		log.Fatalf("server not started: %v", err)
	}
}

func InitGenesisBlock() {
	genesisBlock := Block{0, time.Now(), []string{"genesis block"}, "", "001"}
	Blockchain = append(Blockchain, genesisBlock)
}

func InitServer(logger *logger.Logger) *Server {
	server := &Server{
		Addr:   ":7031",
		Pin:    GetHash("qwerty"),
		router: mux.NewRouter(),
		log:    logger,
	}
	return server
}

func (s *Server) Start() error {
	s.handlerChain()
	s.log.Service("start server with test")
	return http.ListenAndServe(s.Addr, s.router)
}

func (s *Server) handlerChain() {
	s.router = mux.NewRouter()
	s.router.HandleFunc("/get", s.Middleware(s.hGetBlocks))
	s.router.HandleFunc("/gen", s.Middleware(s.hCreateBlock))
	s.router.HandleFunc("/test", s.hChangeBlock)

	s.log.Service("configure route")
}

func (s *Server) hChangeBlock(w http.ResponseWriter, r *http.Request) {
	s.log.Info("change testing block")
	Blockchain[len(Blockchain)-1].BlockData = []string{"not valid data"}
}

// hGetBlocks получение блоков
func (s *Server) hGetBlocks(w http.ResponseWriter, r *http.Request) {
	bytes, err := json.MarshalIndent(Blockchain, "", " ")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	io.WriteString(w, string(bytes))
	s.log.Info("get all blocks")
}

// hCreateBlock создание блока
func (s *Server) hCreateBlock(w http.ResponseWriter, r *http.Request) {
	var req RequestData

	decodeBody := json.NewDecoder(r.Body)
	if err := decodeBody.Decode(&req); err != nil {
		s.log.Warningf("cannot decode request json: %v", err)
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("cannot decode request"))
		return
	}
	defer r.Body.Close()

	oldBlock := Blockchain[len(Blockchain)-1]
	newBlock := GenerateBlock(oldBlock, req.Data)

	if !ValidationBlock(oldBlock, newBlock) {
		s.log.Errorf("cannot generate new block, oldBlock not valid, index: %v", oldBlock.Index)
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("cannot generate new block"))
		return
	}
	newBlockchain := append(Blockchain, newBlock)

	if err := ReplaceChain(newBlockchain); err != nil {
		s.log.Errorf("cannot replace blockchain: %v", err)
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("cannot replace blockchain"))
		return
	}
	responseBlock, err := json.MarshalIndent(newBlock, "", " ")
	if err != nil {
		s.log.Warningf("cannot responce block: %v\n", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		w.Write([]byte("cannot responce block"))
		return
	}
	w.WriteHeader(http.StatusCreated)
	w.Write(responseBlock)
	s.log.Infof("generate new block; index: %v", newBlock.Index)
}

func (s *Server) Middleware(handler http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if GetHash(r.FormValue("pin")) != s.Pin {
			s.log.Warning("pin not valid")
			http.Error(w, "pin not valid", http.StatusForbidden)
			return
		}
		handler(w, r)
	}
}

func GetHash(data string) string {
	h := sha256.New()
	h.Write([]byte(data))
	return string(h.Sum(nil))
}

// GenerateBlock генерация блока
func GenerateBlock(old Block, data string) Block {
	var new Block
	new.Index = old.Index + 1
	new.TimeStamp = time.Now()
	new.PrevHash = old.Hash
	new.BlockData = make([]string, 0)
	new.BlockData = append(new.BlockData, old.BlockData...)
	new.BlockData = append(new.BlockData, data)
	new.Hash = CalculateHash(new)
	return new
}

// CalculateHash создает хеш из данных блока
func CalculateHash(block Block) string {
	recordData := fmt.Sprintf("%v%v%v%v", strconv.Itoa(int(block.Index)), block.TimeStamp.String(), block.BlockData[0], block.PrevHash)
	hash := sha256.New()
	hash.Write([]byte(recordData))
	hashed := hash.Sum(nil)

	return hex.EncodeToString(hashed)
}

// BlockValidation проверка блока
func ValidationBlock(oldBlock, newBlock Block) bool {
	if oldBlock.Hash != newBlock.PrevHash {
		return false
	}
	if oldBlock.Index+1 != newBlock.Index {
		return false
	}
	if len(Blockchain) > 3 {
		if CalculateHash(oldBlock) != oldBlock.Hash {
			return false
		}
		if CalculateHash(newBlock) != newBlock.Hash {
			return false
		}
	}
	return true
}

// ReplaceChain выбираем самую длинную цепочку блоков, как самую актуальную
func ReplaceChain(newChain []Block) error {
	if len(newChain) > len(Blockchain) {
		Blockchain = newChain
		return nil
	}
	return fmt.Errorf("not replace blockchain")
}
